module Store
	class Good
		attr_accessor :price, :name, :product_code
		def initialize(price, kind, product_code)
			@price = price
			@name = kind
			@product_code = product_code
		end
	end

	class Checkout
		def initialize(item_array)
			@basket = Hash.new(:new_aray => {})
			item_array.each do |set_item|
				@basket[set_item.product_code] = {:quantity => 0, :name => set_item.name, :price => set_item.price}
			end
		end

		def scan(item)
			@basket[item.product_code][:quantity] += 1
		end

		def price_sum
			prices = []

			if @basket["CF1"][:quantity] % 2 == 1
				prices << (@basket["CF1"][:quantity] - 1) * (@basket["CF1"][:price] / 2) + @basket["CF1"][:price]
			else
				prices << @basket["CF1"][:quantity] * (@basket["CF1"][:price] / 2) 
			end
			
			if @basket["AP1"][:quantity] > 2 
				prices << @basket["AP1"][:quantity] * 4.50
			else
				prices << @basket["AP1"][:quantity] * @basket["AP1"][:price]
			end

			if @basket["CH1"][:quantity] > 0 && @basket["MK1"][:quantity] > 0
				prices << -@basket["MK1"][:price]
				prices << @basket["CH1"][:quantity] * @basket["CH1"][:price]
			else
				prices << @basket["CH1"][:quantity] * @basket["CH1"][:price]
			end

			if @basket["MK1"][:quantity]
				prices << @basket["MK1"][:quantity] * @basket["MK1"][:price]
			end
			return prices.inject(0){|sum,x| sum + x }.round(2)
		end

		def basket_status
			milk_counter = 0

			coffee_counter = 0
			coffee_counter_two = 0
			text_array = [] #put in array, easier to test

			@basket.each do |k, v|
				i = 0
				while i < v[:quantity]
					i += 1
					if @basket["AP1"][:quantity] > 2 && k == "AP1" 
						text_array << "#{k}       #{'%.2f' % v[:price]}"
						text_array << "APPL       -1.50"
					elsif @basket["AP1"][:quantity] < 3 && k == "AP1"
						text_array << "#{k}       #{'%.2f' % v[:price]}"
					elsif @basket["CF1"][:quantity] % 2 == 1 && k == "CF1"
						if coffee_counter_two < 1
							text_array << "#{k}       #{'%.2f' % v[:price]}"
							coffee_counter_two += 1
						elsif
						  coffee_counter % 2 == 0 && coffee_counter_two > 1
							text_array << "Coffee       -11.23"
							text_array << "#{k}       #{'%.2f' % v[:price]}"
						end
						coffee_counter +=1
						coffee_counter_two +=1
					elsif @basket["CF1"][:quantity] % 2 == 0 && coffee_counter % 2 == 0 && k == "CF1"
						coffee_counter +=1
						text_array << "#{k}       #{'%.2f' % v[:price]}"
						text_array << "Coffee       -11.23"
					elsif @basket["CH1"][:quantity] > 0 && k == "MK1" && milk_counter < 1
						milk_counter +=1
						text_array << "#{k}       #{'%.2f' % v[:price]}"
						text_array << "CHMK       -4.75"
					elsif k == "CH1"
						text_array << "#{k}       #{'%.2f' % v[:price]}"
					elsif k == "MK1"
						text_array << "#{k}       #{'%.2f' % v[:price]}"
					end
				end
			end
			return text_array
		end

	end
end


