require 'minitest/autorun'
require_relative './store'

class TestStore < Minitest::Test
	include Store
	def setup
  	@CH1 = Good.new(3.11, "Chai", "CH1")
		@AP1 = Good.new(6, "Apple", "AP1")
		@CF1 = Good.new(11.23, "Coffee", "CF1")
		@MK1 = Good.new(4.75, "Milk", "MK1")
  	item_array = [@CH1, @AP1, @CF1, @MK1]
  	
  	@checkout = Checkout.new(item_array)

	end

  def test_first_basket 
  	#Basket: CH1, AP1, CF1, MK1; Test Milk, no Chai discount

  	@checkout.scan(@MK1)
    assert_equal 4.75, @checkout.price_sum
  end

  def test_second_basket 
  	#Basket: CH1, AP1, CF1, MK1; Test Chai Milk discount
  	@checkout.scan(@CH1)
  	@checkout.scan(@AP1)
  	@checkout.scan(@CF1)
  	@checkout.scan(@MK1)
    assert_equal 20.34, @checkout.price_sum
  end

  def test_third_basket
  	#Basket: CH1, AP1, CF1, MK1; Test Chai milk discount limit 1
  	@checkout.scan(@CH1)
  	@checkout.scan(@AP1)
  	@checkout.scan(@CF1)
  	@checkout.scan(@MK1)
  	@checkout.scan(@MK1)
    assert_equal 25.09, @checkout.price_sum
  end

  def test_fourth_basket
  	#Basket: MK1, AP1
  	@checkout.scan(@MK1)
  	@checkout.scan(@AP1)
    assert_equal 10.75, @checkout.price_sum
  end

  def test_fifth_basket
    #Basket: AP1, AP1, CH1, AP1
  	@checkout.scan(@AP1)
  	@checkout.scan(@AP1)
  	@checkout.scan(@CH1)
  	@checkout.scan(@AP1)
    assert_equal 16.61, @checkout.price_sum
  end

  def test_sixth_basket
    #Basket: CF1
  	@checkout.scan(@CF1)
    assert_equal 11.23, @checkout.price_sum
  end

  def test_seventh_basket
  	#Basket: CF1, CF1; Test coffee buy one get one free
  	@checkout.scan(@CF1)
  	@checkout.scan(@CF1)
    assert_equal 11.23, @checkout.price_sum
  end

  def test_eighth_basket
    #Basket: CF1, CF1, CF1; Test coffee buy one get one free no limit
  	@checkout.scan(@CF1)
  	@checkout.scan(@CF1)
  	@checkout.scan(@CF1)
  	@checkout.scan(@CF1)
  	@checkout.scan(@CF1)
    assert_equal 33.69, @checkout.price_sum
  end

  def test_ninth_basket
    #Basket: AP1; Apple price when less than three is $6.
  	@checkout.scan(@AP1)
    assert_equal 6.00, @checkout.price_sum
  end

  def test_tenth_basket
    #Basket: AP1, AP1, AP1; Test apple price drops to 4.50 when three or more purchased
  	@checkout.scan(@AP1)
  	@checkout.scan(@AP1)
  	@checkout.scan(@AP1)
    assert_equal 13.50, @checkout.price_sum
  end

  def test_first_basket_status
    #Basket: CF1; one item basket
  	@checkout.scan(@CF1)

    assert_equal ["CF1       11.23"], @checkout.basket_status
  end

  def test_second_basket_status
    #Basket: AP1, AP1, CH1, AP1; Test for two apples.

  	@checkout.scan(@AP1)
  	@checkout.scan(@AP1)


  	
    assert_equal ["AP1       6.00", "AP1       6.00"], @checkout.basket_status
  end

  def test_third_basket_status
    #Basket: CF1; Test one chai, one milk basket with milk discount
  	@checkout.scan(@MK1)
  	@checkout.scan(@CH1)
  	
    assert_equal ["CH1       3.11", "MK1       4.75", "CHMK       -4.75"], @checkout.basket_status
  end

  def test_fourth_basket_status
    #Basket: CF1; Test one chai, one milk basket with milk discount only applies to one milk
  	@checkout.scan(@MK1)
  	@checkout.scan(@MK1)
  	@checkout.scan(@CH1)
  	
    assert_equal ["CH1       3.11", "MK1       4.75", "CHMK       -4.75", "MK1       4.75"], @checkout.basket_status
  end

  def test_fifth_basket_status
    #Basket: CH1, AP1; two item coffee basket 
  	@checkout.scan(@CF1)
  	@checkout.scan(@CF1)

    assert_equal ["CF1       11.23", "Coffee       -11.23"], @checkout.basket_status
  end

  def test_sixth_basket_status
    #Basket: CH1, AP1; three item coffee basket 
  	@checkout.scan(@CF1)
  	@checkout.scan(@CF1)
  	@checkout.scan(@CF1)

    assert_equal ["CF1       11.23", "Coffee       -11.23", "CF1       11.23"], @checkout.basket_status
  end

  def test_seventh_basket_status
    #Basket: CH1, AP1; nine item coffee basket 
  	@checkout.scan(@CF1)
  	@checkout.scan(@CF1)
  	@checkout.scan(@CF1)
  	@checkout.scan(@CF1)
  	@checkout.scan(@CF1)
  	@checkout.scan(@CF1)
  	@checkout.scan(@CF1)
  	@checkout.scan(@CF1)
  	@checkout.scan(@CF1)

    assert_equal ["CF1       11.23", "Coffee       -11.23", "CF1       11.23", "Coffee       -11.23", "CF1       11.23", "Coffee       -11.23", "CF1       11.23","Coffee       -11.23", "CF1       11.23"], @checkout.basket_status
  end

  def test_eighth_basket_status
    #Basket: CH1, AP1; two item basket 
  	@checkout.scan(@CH1)
  	@checkout.scan(@AP1)

    assert_equal ["CH1       3.11", "AP1       6.00"], @checkout.basket_status
  end

  def test_ninth_basket_status
    #Basket: AP1, AP1, CH1, AP1; Test for three apples.
  	@checkout.scan(@CH1)
  	@checkout.scan(@AP1)
  	@checkout.scan(@AP1)
  	@checkout.scan(@AP1)
  	@checkout.scan(@MK1)
  	
    assert_equal ["CH1       3.11", "AP1       6.00", "APPL       -1.50", "AP1       6.00", "APPL       -1.50","AP1       6.00", "APPL       -1.50", "MK1       4.75", "CHMK       -4.75"], @checkout.basket_status
  end

end